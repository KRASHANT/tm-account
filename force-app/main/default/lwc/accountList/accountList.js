import { NavigationMixin, CurrentPageReference } from 'lightning/navigation';
import { fireEvent } from 'c/pubsub';
import { loadStyle } from 'lightning/platformResourceLoader';
import ursusResources from '@salesforce/resourceUrl/ursus_park';
import { LightningElement, track, wire } from 'lwc';
/** accountController.searchaccounts(searchTerm) Apex method */
import searchAccounts from '@salesforce/apex/accountController.searchAccounts';
export default class AccountListNav extends NavigationMixin(LightningElement) {
	@track searchTerm = '';
  @track accounts;
  @wire(CurrentPageReference) pageRef;
  @wire(searchAccounts, {searchTerm: '$searchTerm'})
  loadAccounts(result) {
    this.accounts = result;
    if (result.data) {
      fireEvent(this.pageRef, 'accountListUpdate', result.data);
    }
  }
  
  connectedCallback() {
		loadStyle(this, ursusResources + '/style.css');
	}
	handleSearchTermChange(event) {
		// Debouncing this method: do not update the reactive property as
		// long as this function is being called within a delay of 300 ms.
		// This is to avoid a very large number of Apex method calls.
		window.clearTimeout(this.delayTimeout);
		const searchTerm = event.target.value;
		// eslint-disable-next-line @lwc/lwc/no-async-operation
		this.delayTimeout = setTimeout(() => {
			this.searchTerm = searchTerm;
		}, 300);
	}
	get hasResults() {
		return (this.accounts.data.length > 0);
	}
	handleAccountView(event) {
		// Get account record id from accountview event
		const accountId = event.detail;
		// Navigate to account record page
		this[NavigationMixin.Navigate]({
			type: 'standard__recordPage',
			attributes: {
				recordId: accountId,
				objectApiName: 'Account',
				actionName: 'view',
			},
		});
	}
}