import { LightningElement, api } from 'lwc';

export default class accountTile extends LightningElement {
	@api account;
  
  handleOpenRecordClick() {
    const selectEvent = new CustomEvent('accountview', {
      detail: this.account.Id
    });
    this.dispatchEvent(selectEvent);
    console.log('EVENT ' + selectEvent);
  }
}