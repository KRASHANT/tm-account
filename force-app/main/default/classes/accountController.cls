public with sharing class accountController {
    @AuraEnabled(Cacheable=true)
    public static Account[] getAllAccounts() {
        return [SELECT Id, Name, Phone, Industry
            FROM Account ORDER BY Name LIMIT 50];
    }

    @AuraEnabled(Cacheable=true)
    public static Account[] searchAccounts(String searchTerm) {
        // Return all Accounts when no search term
        searchTerm = searchTerm.trim();
        if (searchTerm == '') {
            return getAllAccounts();
        }
        // Prepare query paramters
        searchTerm = '%'+ searchTerm +'%';
        // Execute search query
        return [SELECT Id, Name, Phone, Industry FROM Account WHERE Name LIKE :searchTerm ORDER BY Name LIMIT 50];
    }
}